import pickle
from copy import deepcopy
import os
import torch
from torch import nn
from torchvision import models
from fastai.vision import *
from fastai.basics import *

from resnet50 import identity_resnet50


def mkdir(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
        

def make_original_model():
    ref_model = models.resnet50(pretrained=True)
    model = identity_resnet50()
    ref_modules = [] 
    for elem in ref_model.modules():
        if isinstance(elem, nn.Conv2d) or isinstance(elem, nn.Linear) or isinstance(elem, nn.BatchNorm2d):
            model.add_layer(elem)
    return model


def checkpoint(ind, learner, orig_shape, n_params, rank, sparsity, decomposition_error, compression, total_compression, acc, top5_acc, checkpoint_folder): 
    if type(acc) == torch.Tensor:
        acc = acc.numpy().flatten()[0]
    if type(top5_acc) == torch.Tensor:
        top5_acc = top5_acc.numpy().flatten()[0]

    logfilename = os.path.join(checkpoint_folder, 'log.txt')
    mkdir(logfilename)
    with open(logfilename, 'a') as f:
        f.write(f"\n{ind}\n" +
                f"\tn_layer_params={n_params}\n" + 
                f"\torig_shape={orig_shape}\n" + 
                f"\trank={rank}\n" + 
                f"\tsparsity={sparsity}\n" + 
                f"\tdecomposition_error={decomposition_error}\n" + 
                f"\tcompression={compression}\n" +
                f"\ttotal_compression={total_compression}\n" + 
                f"\taccuracy={acc}\n" + 
                f"\ttop5_acc={top5_acc}\n")

    filename = os.path.join(checkpoint_folder, f'modules/{ind:03}.pkl')
    mkdir(filename)
    with open(filename, 'wb') as f:
        pickle.dump(get_layer_by_ind(learner, ind), f)

    filename = os.path.join(checkpoint_folder, f'models/{ind:03}')
    mkdir(filename)
    learner.save(filename)

    filename = os.path.join(checkpoint_folder, f'arch/{ind:03}.txt')
    mkdir(filename)
    with open(filename, 'w') as f:
        f.write(str(learner.model))
        f.write('\n'*3 + '='*30 + '\n'*3)
        try:
            f.write(learner.summary())
        except:
            pass


def load_checkpoint(checkpoint_folder, data, ind=-1):
    """
    ind: if -1, then original model is loaded for. for loading maximum available model pass large index
    """
    modules_folder = os.path.join(checkpoint_folder, 'modules')
    mkdir(os.path.join(modules_folder, 'dummy.txt'))
    model = make_original_model()
    maxind = -1
    for elem in os.listdir(modules_folder):
        with open(os.path.join(modules_folder, elem), 'rb') as f:
            i = int(elem[:-4])
            if i <= ind:
                maxind = max(maxind, i)
                model.set_layer_by_ind(pickle.load(f), i)

    ind = maxind
    model = model.cuda()

    def dummy_model_constructor(*args, **hkwargs):
        return model 

    learner = cnn_learner(data, dummy_model_constructor, metrics=[accuracy, top_k_accuracy])
    models_folder = os.path.join(checkpoint_folder, 'models')
    mkdir(os.path.join(models_folder, 'dummy.txt'))
    if ind >= 0:
        learner_checkpoint = os.path.join(models_folder, f'{ind:03}')
    else:
        learner_checkpoint = os.path.join(checkpoint_folder, f'ref_learner')

    learner.load(learner_checkpoint)

    arch_folder = os.path.join(checkpoint_folder, 'arch')
    mkdir(os.path.join(arch_folder, 'dummy.txt'))

    return ind, learner 


def total_compression(total_params, total_compression, layer_params, layer_compression):
    """
    compression in the format n_original/n_compressed.
    params are just numbers of corresponding params
    """
    total_compression = 1/total_compression
    layer_compression = 1/layer_compression

    return 1/(total_compression - (1 - layer_compression)*layer_params/total_params)


def set_layer_by_ind(learner, layer, ind, trainable=True):

    model = learner.model

    if trainable:
        for p in layer.parameters():
            p.requires_grad = True

    if ind == 0:
        model[0][0] = layer

    #layer1
    elif ind == 1:
        model[0][4][0].conv1 = layer
    elif ind == 2:
        model[0][4][0].conv2 = layer
    elif ind == 3:
        model[0][4][0].conv3 = layer
    elif ind == 4:
        model[0][4][0].downsample[0] = layer

    elif ind == 5:
        model[0][4][1].conv1 = layer
    elif ind == 6:
        model[0][4][1].conv2 = layer
    elif ind == 7:
        model[0][4][1].conv3 = layer

    elif ind == 8:
        model[0][4][2].conv1 = layer
    elif ind == 9:
        model[0][4][2].conv2 = layer
    elif ind == 10:
        model[0][4][2].conv3 = layer

    # layer2
    elif ind == 11:
        model[0][5][0].conv1 = layer
    elif ind == 12:
        model[0][5][0].conv2 = layer
    elif ind == 13:
        model[0][5][0].conv3 = layer
    elif ind == 14:
        model[0][5][0].downsample[0] = layer

    elif ind == 15:
        model[0][5][1].conv1 = layer
    elif ind == 16:
        model[0][5][1].conv2 = layer
    elif ind == 17:
        model[0][5][1].conv3 = layer

    elif ind == 18:
        model[0][5][2].conv1 = layer
    elif ind == 19:
        model[0][5][2].conv2 = layer
    elif ind == 20:
        model[0][5][2].conv3 = layer

    elif ind == 21:
        model[0][5][3].conv1 = layer
    elif ind == 22:
        model[0][5][3].conv2 = layer
    elif ind == 23:
        model[0][5][3].conv3 = layer

    # layer3
    elif ind == 24:
        model[0][6][0].conv1 = layer
    elif ind == 25:
        model[0][6][0].conv2 = layer
    elif ind == 26:
        model[0][6][0].conv3 = layer
    elif ind == 27:
        model[0][6][0].downsample[0] = layer

    elif ind == 28:
        model[0][6][1].conv1 = layer
    elif ind == 29:
        model[0][6][1].conv2 = layer
    elif ind == 30:
        model[0][6][1].conv3 = layer

    elif ind == 31:
        model[0][6][2].conv1 = layer
    elif ind == 32:
        model[0][6][2].conv2 = layer
    elif ind == 33:
        model[0][6][2].conv3 = layer

    elif ind == 34:
        model[0][6][3].conv1 = layer
    elif ind == 35:
        model[0][6][3].conv2 = layer
    elif ind == 36:
        model[0][6][3].conv3 = layer

    elif ind == 37:
        model[0][6][4].conv1 = layer
    elif ind == 38:
        model[0][6][4].conv2 = layer
    elif ind == 39:
        model[0][6][4].conv3 = layer

    elif ind == 40:
        model[0][6][5].conv1 = layer
    elif ind == 41:
        model[0][6][5].conv2 = layer
    elif ind == 42:
        model[0][6][5].conv3 = layer

    #layer4
    elif ind == 43:
        model[0][7][0].conv1 = layer
    elif ind == 44:
        model[0][7][0].conv2 = layer
    elif ind == 45:
        model[0][7][0].conv3 = layer
    elif ind == 46:
        model[0][7][0].downsample[0] = layer

    elif ind == 47:
        model[0][7][1].conv1 = layer
    elif ind == 48:
        model[0][7][1].conv2 = layer
    elif ind == 49:
        model[0][7][1].conv3 = layer

    elif ind == 50:
        model[0][7][2].conv1 = layer
    elif ind == 51:
        model[0][7][2].conv2 = layer
    elif ind == 52:
        model[0][7][2].conv3 = layer

    else:
        raise Exception('BadIndex')


def get_layer_by_ind(model, ind):

#     if type(model) not in [nn.Module, nn.Sequential]:
#         model = model.module

    if ind == 0:
        return model[0][0]

    #layer1
    elif ind == 1:
        return model[0][4][0].conv1
    elif ind == 2:
        return model[0][4][0].conv2
    elif ind == 3:
        return model[0][4][0].conv3
    elif ind == 4:
        return model[0][4][0].downsample[0]

    elif ind == 5:
        return model[0][4][1].conv1
    elif ind == 6:
        return model[0][4][1].conv2
    elif ind == 7:
        return model[0][4][1].conv3

    elif ind == 8:
        return model[0][4][2].conv1
    elif ind == 9:
        return model[0][4][2].conv2
    elif ind == 10:
        return model[0][4][2].conv3

    # layer2
    elif ind == 11:
        return model[0][5][0].conv1
    elif ind == 12:
        return model[0][5][0].conv2
    elif ind == 13:
        return model[0][5][0].conv3
    elif ind == 14:
        return model[0][5][0].downsample[0]

    elif ind == 15:
        return model[0][5][1].conv1
    elif ind == 16:
        return model[0][5][1].conv2
    elif ind == 17:
        return model[0][5][1].conv3

    elif ind == 18:
        return model[0][5][2].conv1
    elif ind == 19:
        return model[0][5][2].conv2
    elif ind == 20:
        return model[0][5][2].conv3

    elif ind == 21:
        return model[0][5][3].conv1
    elif ind == 22:
        return model[0][5][3].conv2
    elif ind == 23:
        return model[0][5][3].conv3

    # layer3
    elif ind == 24:
        return model[0][6][0].conv1
    elif ind == 25:
        return model[0][6][0].conv2
    elif ind == 26:
        return model[0][6][0].conv3
    elif ind == 27:
        return model[0][6][0].downsample[0]

    elif ind == 28:
        return model[0][6][1].conv1
    elif ind == 29:
        return model[0][6][1].conv2
    elif ind == 30:
        return model[0][6][1].conv3

    elif ind == 31:
        return model[0][6][2].conv1
    elif ind == 32:
        return model[0][6][2].conv2
    elif ind == 33:
        return model[0][6][2].conv3

    elif ind == 34:
        return model[0][6][3].conv1
    elif ind == 35:
        return model[0][6][3].conv2
    elif ind == 36:
        return model[0][6][3].conv3

    elif ind == 37:
        return model[0][6][4].conv1
    elif ind == 38:
        return model[0][6][4].conv2
    elif ind == 39:
        return model[0][6][4].conv3

    elif ind == 40:
        return model[0][6][5].conv1
    elif ind == 41:
        return model[0][6][5].conv2
    elif ind == 42:
        return model[0][6][5].conv3

    #layer4
    elif ind == 43:
        return model[0][7][0].conv1
    elif ind == 44:
        return model[0][7][0].conv2
    elif ind == 45:
        return model[0][7][0].conv3
    elif ind == 46:
        return model[0][7][0].downsample[0]

    elif ind == 47:
        return model[0][7][1].conv1
    elif ind == 48:
        return model[0][7][1].conv2
    elif ind == 49:
        return model[0][7][1].conv3

    elif ind == 50:
        return model[0][7][2].conv1
    elif ind == 51:
        return model[0][7][2].conv2
    elif ind == 52:
        return model[0][7][2].conv3

    else:
        raise Exception('BadIndex')


def temp_checkpoint(ind, learner, checkpoint_folder):
    filename = os.path.join(checkpoint_folder, f'temp/module_{ind:03}.pkl')
    mkdir(filename)
    with open(filename, 'wb') as f:
        pickle.dump(get_layer_by_ind(learner, ind), f)

    filename = os.path.join(checkpoint_folder, f'temp/model_{ind:03}')
    mkdir(filename)
    learner.save(filename)


def load_temp_checkpoint(checkpoint_folder, data):
    modules_folder = os.path.join(checkpoint_folder, 'modules')
    model = make_original_model()
    maxind = -1
    for elem in os.listdir(modules_folder):
        with open(os.path.join(modules_folder, elem), 'rb') as f:
            i = int(elem[:-4])
            maxind = max(maxind, i)
            model.set_layer_by_ind(pickle.load(f), i)

    ind = maxind
    temp_folder = os.path.join(checkpoint_folder, 'temp')
    with open(os.path.join(temp_folder, f'module_{ind+1:03}.pkl'), 'rb') as f:
        model.set_layer_by_ind(pickle.load(f), ind + 1)

    model = model.cuda()

    def dummy_model_constructor(*args, **hkwargs):
        return model

    learner = cnn_learner(data, dummy_model_constructor, metrics=[accuracy, top_k_accuracy])
    learner_checkpoint = os.path.join(temp_folder, f'model_{ind+1:03}')

    learner.load(learner_checkpoint)

    return ind, learner
