'''
This script allow us to form learning scenarios for one layer decompositions with different error bounds
It helps us to test how different layers withstand different decomposition error rates
'''
import argparse
import os
import shutil
import pandas as pd
import yaml

from utils import bcolors

parser = argparse.ArgumentParser(description='Choose decomposed layers verisons')

parser.add_argument('layer_no', type=int, help='layer to decompose')
parser.add_argument('--results', metavar='DIR', required=True,
                    help='path where decomposition results')
parser.add_argument('--out', type=str, required=True, help='where to store results')
parser.add_argument('--sparsity', type=float, default=0.01, help='required sparsity level')
parser.add_argument('--error_low', type=float, default=0.3, help='decomposition error bound')
parser.add_argument('--error_high', type=float, default=0.7, help='decomposition error bound')
parser.add_argument('--error_step', type=float, default=0.05, help='decomposition error bound')
args = parser.parse_args()

def main():
    assert args.layer_no >= 0 and args.layer_no <= 52 #numbers of layers in resnet50

    if os.path.exists(args.out):
        while True:
            print (f'{bcolors.WARNING}Output dir exists. are you shure you want to overwrite it? (Y/n) {bcolors.ENDC}')
            ret = input()
            if ret in ['Y', 'y']:
                shutil.rmtree(args.out)
                break
            if ret in ['N', 'n']:
                return

    os.makedirs(args.out)
    with open(os.path.join(args.out, 'info.txt'), 'w') as f:
        yaml.dump({'error_low': args.error_low,
                   'error_high': args.error_high,
                   'error_step': args.error_step,
                   'sparsity': args.sparsity,
                   'layer_no': args.layer_no}, f)


    layer_path = os.path.join(args.results, str(args.layer_no))
    weight_path = None
    if os.path.exists(layer_path):
        metapath = os.path.join(layer_path, 'meta.tsv')
        meta = pd.read_csv(metapath, sep='\t')
        meta = meta[(meta['sparsity'] == args.sparsity) & (meta['error'] < args.error_high) & (meta['error'] > args.error_low)]
        meta = meta.sort_values('error')
        last_val = None 
        for ind, row in meta.iterrows():
            if last_val is None or abs(row['error'] - last_val) >= args.error_step:
                last_val = row['error']
                weight_path = f'{args.layer_no}/{ind}.pkl'

                if weight_path is None:
                    print(f'required decompositions for layer {args.layer_no} do not exist!')
                    weight_path = ''

                with open(os.path.join(args.out, f'arch__{last_val:.3f}.txt'), 'a') as f:
                    f.write('\n'*args.layer_no)
                    f.write(weight_path + '\n')
                    f.write('\n'*(52 - args.layer_no))

if __name__ == '__main__':
    main()

        

     
