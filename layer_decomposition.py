from tensorly.decomposition import parafac
from sparse_parafac import sparse_parafac
import torch
import torch.nn as nn
import numpy as np
from copy import deepcopy
import pickle
import os

def layer_from_pickle(path):
    with open(path, 'rb') as f:
        last, first, vertical, horizontal, S = pickle.load(f)
    original_layer_path = os.path.join(os.path.dirname(path), 'original_layer.pkl') 
    with open(original_layer_path, 'rb') as f:
        original_layer = pickle.load(f)
    return matrices_to_layer(last, first, vertical, horizontal, original_layer, S)


class SparseConv2d(nn.Module):
    '''
    This is dummy implementation for sparse convolution
    '''
    def __init__(self, low_rank, sparse):
        super(SparseConv2d, self).__init__()
        self.low_rank = low_rank
        self.sparse = sparse
        S = sparse.weight.detach().cpu().numpy() != 0
#         self.mask = torch.tensor(np.array(S != 0, dtype=np.int), requires_grad=False).float().cuda()
        self.mask = torch.nn.Parameter(torch.tensor(np.array(S != 0, dtype=np.int), requires_grad=False).float())
#        self.mask = torch.tensor(np.array(S != 0, dtype=np.int), requires_grad=False).float()
        self.sparse.weight.register_hook(lambda grad: grad*self.mask)
        self.ignore_sparse=False

    def forward(self, x):
        #self.sparse.weight = torch.nn.Parameter(self.mask * self.sparse.weight)
#         self.sparse.weight.data = self.mask * self.sparse.weight.data

        return self.low_rank(x) + self.sparse(x) + 0 * self.mask.sum()

#         if not self.ignore_sparse:
#             return self.low_rank(x) + self.sparse(x) + 0 * self.mask.sum()
#         return self.low_rank(x)

def matrices_to_layer(last, first, vertical, horizontal, layer, S=None):
    """
    layer: original layer, we need to copy parameters and bias 
    """
    last, first, vertical, horizontal = \
        [torch.tensor(x) for x in (last, first, vertical, horizontal)]

    pointwise_s_to_r_layer = torch.nn.Conv2d(in_channels=first.shape[0],
            out_channels=first.shape[1], kernel_size=1, stride=1, padding=0,
            dilation=layer.dilation, bias=False)

    depthwise_vertical_layer = torch.nn.Conv2d(in_channels=vertical.shape[1],
            out_channels=vertical.shape[1], kernel_size=(vertical.shape[0], 1),
            stride=1, padding=(layer.padding[0], 0), dilation=layer.dilation,
            groups=vertical.shape[1], bias=False)

    depthwise_horizontal_layer = \
        torch.nn.Conv2d(in_channels=horizontal.shape[1],
            out_channels=horizontal.shape[1],
            kernel_size=(1, horizontal.shape[0]), stride=layer.stride,
            padding=(0, layer.padding[0]),
            dilation=layer.dilation, groups=horizontal.shape[1], bias=False)

    if S is None:
        pointwise_r_to_t_layer = torch.nn.Conv2d(in_channels=last.shape[1],
                out_channels=last.shape[0], kernel_size=1, stride=1,
                padding=0, dilation=layer.dilation, bias=True)

        if bias is not None:
            pointwise_r_to_t_layer.bias.data = layer.bias.data
    else:
        pointwise_r_to_t_layer = torch.nn.Conv2d(in_channels=last.shape[1],
                out_channels=last.shape[0], kernel_size=1, stride=1,
                padding=0, dilation=layer.dilation, bias=False)

        sparse_layer = deepcopy(layer)
        sparse_layer.weight = nn.Parameter(torch.tensor(S))

        if layer.bias is not None:
            sparse_layer.bias.data = layer.bias.data

    depthwise_horizontal_layer.weight.data = \
        torch.transpose(horizontal, 1, 0).unsqueeze(1).unsqueeze(1)
    depthwise_vertical_layer.weight.data = \
        torch.transpose(vertical, 1, 0).unsqueeze(1).unsqueeze(-1)
    pointwise_s_to_r_layer.weight.data = \
        torch.transpose(first, 1, 0).unsqueeze(-1).unsqueeze(-1)
    pointwise_r_to_t_layer.weight.data = last.unsqueeze(-1).unsqueeze(-1)

    new_layers = [pointwise_s_to_r_layer, depthwise_vertical_layer,
                    depthwise_horizontal_layer, pointwise_r_to_t_layer]
    low_rank = nn.Sequential(*new_layers)

    if S is None:
        return low_rank
    else:
        return SparseConv2d(low_rank, sparse_layer)



def cp_decomposition_conv_layer(layer, treshold=0.15, sparse=False, sparsity_coeff=0.05, rank_step=1):
    """ Gets a conv layer and a target rank,
        returns a nn.Sequential object with the decomposition """

    # Perform CP decomposition on the layer weight tensorly.
    n_params = None
    for rank in range(2, int(3*np.prod(layer.weight.shape)/sum(layer.weight.shape)), rank_step):
        if sparse:
            (last, first, vertical, horizontal), S, errs = sparse_parafac(layer.weight.data.cpu().numpy(), rank=rank,
                                                                          init='random',
                                                                 return_errors=True, card=sparsity_coeff)
            n_params = (sum(layer.weight.shape)*rank + (len(layer.weight.shape)+1)*sparsity_coeff*np.prod(layer.weight.shape))
            compression = np.prod(layer.weight.shape)/n_params
        else:
            (last, first, vertical, horizontal), errs = parafac(layer.weight.data.cpu().numpy(), rank=rank, init='random',
                                                                 return_errors=True)
            S = None
            n_params = sum(layer.weight.shape)*rank
            compression = np.prod(layer.weight.shape)/n_params
        err = errs[-1]
#         if compression <= 0.5:
        if compression <= 1:
            return deepcopy(layer), None, 0, 1
        if err < treshold:
            break

    dlayer = matrices_to_layer(last, first, vertical, horizontal, layer, S) 
    return dlayer, rank, err, compression, np.prod(layer.weight.shape)



def get_weights_from_decomposed_layer(layer):
    first = layer.low_rank[0].weight.data
    vertical = layer.low_rank[1].weight.data
    horizontal = layer.low_rank[2].weight.data
    last = layer.low_rank[3].weight.data
    if type(layer) is SparseConv2d:
        sparse = layer.sparse.weight.data
        return last, first, vertical, horizontal, sparse
    return last, first, vertical, horizontal


def decomposition_from_weights_conv_layer(layer, last, first, vertical, horizontal, sparse_weight=None):
    """ returns module for decomposition of given rank """

    pointwise_s_to_r_layer = torch.nn.Conv2d(in_channels=first.shape[1],
                                             out_channels=first.shape[0], kernel_size=1, stride=1, padding=0,
                                             dilation=layer.dilation, bias=False)

    depthwise_vertical_layer = torch.nn.Conv2d(in_channels=vertical.shape[0],
                                               out_channels=vertical.shape[0], kernel_size=(vertical.shape[1], 1),
                                               stride=1, padding=(layer.padding[0], 0), dilation=layer.dilation,
                                               groups=vertical.shape[0], bias=False)

    depthwise_horizontal_layer = \
        torch.nn.Conv2d(in_channels=horizontal.shape[0],
                        out_channels=horizontal.shape[0],
                        kernel_size=(1, horizontal.shape[1]), stride=layer.stride,
                        padding=(0, layer.padding[0]),
                        dilation=layer.dilation, groups=horizontal.shape[0], bias=False)

    if sparse_weight is None:
        pointwise_r_to_t_layer = torch.nn.Conv2d(in_channels=last.shape[0],
                                                 out_channels=last.shape[1], kernel_size=1, stride=1,
                                                 padding=0, dilation=layer.dilation, bias=True)

        if layer.bias is not None:
            pointwise_r_to_t_layer.bias.data = layer.bias.data
    else:
        pointwise_r_to_t_layer = torch.nn.Conv2d(in_channels=last.shape[0],
                                                 out_channels=last.shape[1], kernel_size=1, stride=1,
                                                 padding=0, dilation=layer.dilation, bias=False)

        sparse_layer = deepcopy(layer)
        sparse_layer.weight.data = sparse_weight

    depthwise_horizontal_layer.weight.data = horizontal
    depthwise_vertical_layer.weight.data = vertical
    pointwise_s_to_r_layer.weight.data = first
    pointwise_r_to_t_layer.weight.data = last

    new_layers = [pointwise_s_to_r_layer, depthwise_vertical_layer,
                  depthwise_horizontal_layer, pointwise_r_to_t_layer]
    low_rank = nn.Sequential(*new_layers)
    if sparse_weight is None:
        return low_rank
    else:
        return SparseConv2d(low_rank, sparse_layer)
