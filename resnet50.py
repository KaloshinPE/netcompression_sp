import torch.nn as nn
import torch


class IdentityModule(nn.Module):
    def forward(self, inputs):
        return inputs


class Flatten(nn.Module):

    def __init__(self):
        super(Flatten, self).__init__()

    def forward(self, x):
        shape = torch.prod(torch.tensor(x.shape[1:])).item()
        return x.view(-1, shape)


def conv3x3(in_planes, out_planes, stride=1, groups=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, groups=groups, bias=False)


def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None, groups=1, norm_layer=None):
        super(Bottleneck, self).__init__()
        if norm_layer is None:
            self.norm_layer = nn.BatchNorm2d
        else:
            self.norm_layer = norm_layer
        # Both self.conv2 and self.downsample layers downsample the input when stride != 1
        self.conv1 = IdentityModule()
        self.bn1 = IdentityModule()
        self.conv2 = IdentityModule()
        self.bn2 = IdentityModule()
        self.conv3 = IdentityModule()
        self.bn3 = IdentityModule()
        self.relu = IdentityModule()
        self.downsample = downsample
        self.stride = stride
        self.planes = planes
        self.counter = 0

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            if not isinstance(self.downsample[0], IdentityModule):
                out += self.downsample(x)
        elif not isinstance(self.conv3, IdentityModule):
            out += identity

        out = self.relu(out)

        return out

    def add_layer(self, layer):
        """
        returns True if all layers are filled
        """
        if self.counter == 0:
            self.conv1 = layer
        elif self.counter == 0.5:
            self.bn1 = layer

        elif self.counter == 1:
            self.conv2 = layer
        elif self.counter == 1.5:
            self.bn2 = layer

        elif self.counter == 2:
            self.conv3 = layer
        elif self.counter == 2.5:
            self.bn3 = layer

            self.relu = nn.ReLU(inplace=True)
        elif self.counter == 3 and self.downsample is not None:
            self.downsample[0] = layer
        elif self.counter == 3.5 and self.downsample is not None:
            self.downsample[1] = layer

        elif self.counter >= 3:
            return True

        self.counter += 0.5
        return False

    def add_layer_by_ind(self, layer, ind):
        if ind == 0:
            self.conv1 = layer

        elif ind == 1:
            self.conv2 = layer

        elif ind == 2:
            self.conv3 = layer

        elif ind == 3 and self.downsample is not None:
            self.downsample[0] = layer

        else:
            raise Exception('BadIndex')

    def get_layer_by_ind(self, ind):
        if ind == 0:
            return self.conv1

        elif ind == 1:
            return self.conv2

        elif ind == 2:
            return self.conv3

        elif ind == 3 and self.downsample is not None:
            return self.downsample[0]

        else:
            raise Exception('BadIndex')



class ResNet(nn.Module):

    def __init__(self, block, layers, num_classes=1000, zero_init_residual=False,
                 groups=1, width_per_group=64, norm_layer=None):
        super(ResNet, self).__init__()
        if norm_layer is None:
            self.norm_layer = nn.BatchNorm2d
        else:
            self.norm_layer = norm_layer
        self.planes = [int(width_per_group * groups * 2 ** i) for i in range(4)]
        self.inplanes = self.planes[0]
        self.conv1 = IdentityModule()
        self.bn1 = IdentityModule()
        self.relu = IdentityModule()
        self.maxpool = IdentityModule()
        self.layer1 = self._make_layer(block, self.planes[0], layers[0],
                                       groups=groups, norm_layer=self.norm_layer)
        self.layer2 = self._make_layer(block, self.planes[1], layers[1], stride=2,
                                       groups=groups, norm_layer=self.norm_layer)
        self.layer3 = self._make_layer(block, self.planes[2], layers[2], stride=2,
                                       groups=groups, norm_layer=self.norm_layer)
        self.layer4 = self._make_layer(block, self.planes[3], layers[3], stride=2,
                                       groups=groups, norm_layer=self.norm_layer)
        self.avgpool = IdentityModule()
        self.fc = IdentityModule()
        self.counter = 0

    def _make_layer(self, block, planes, blocks, stride=1, groups=1, norm_layer=None):
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                IdentityModule(), IdentityModule()
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, groups, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, stride, None, groups, norm_layer))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        if not isinstance(self.fc, IdentityModule):
            x = x.view(x.size(0), -1)
            x = self.fc(x)
        else:
            x = Flatten()(x)

        return x

    def add_layer(self, layer):
        """
        returns True if all layers are filled
        """

        if isinstance(self.conv1, IdentityModule):
            self.conv1 = layer
            self.counter += 1
            return False

        if isinstance(self.bn1, IdentityModule):
            self.bn1 = layer
            self.relu = nn.ReLU(inplace=True)
            self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
            self.counter += 1
            return False

        self.inplanes = self.planes[0]
        for l in [self.layer1, self.layer2, self.layer3, self.layer4]:
            for bnk in l:
                if not bnk.add_layer(layer):
                    self.counter += 1
                    return False

        if isinstance(self.fc, IdentityModule):
            self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
            self.fc = layer
            self.counter += 1

        return True

    def set_layer_by_ind(self, layer, ind):

        if ind == 0:
            self.conv1 = layer

        #layer1
        elif ind == 1:
            self.layer1[0].conv1 = layer
        elif ind == 2:
            self.layer1[0].conv2 = layer
        elif ind == 3:
            self.layer1[0].conv3 = layer
        elif ind == 4:
            self.layer1[0].downsample[0] = layer

        elif ind == 5:
            self.layer1[1].conv1 = layer
        elif ind == 6:
            self.layer1[1].conv2 = layer
        elif ind == 7:
            self.layer1[1].conv3 = layer

        elif ind == 8:
            self.layer1[2].conv1 = layer
        elif ind == 9:
            self.layer1[2].conv2 = layer
        elif ind == 10:
            self.layer1[2].conv3 = layer

        # layer2
        elif ind == 11:
            self.layer2[0].conv1 = layer
        elif ind == 12:
            self.layer2[0].conv2 = layer
        elif ind == 13:
            self.layer2[0].conv3 = layer
        elif ind == 14:
            self.layer2[0].downsample[0] = layer

        elif ind == 15:
            self.layer2[1].conv1 = layer
        elif ind == 16:
            self.layer2[1].conv2 = layer
        elif ind == 17:
            self.layer2[1].conv3 = layer

        elif ind == 18:
            self.layer2[2].conv1 = layer
        elif ind == 19:
            self.layer2[2].conv2 = layer
        elif ind == 20:
            self.layer2[2].conv3 = layer

        elif ind == 21:
            self.layer2[3].conv1 = layer
        elif ind == 22:
            self.layer2[3].conv2 = layer
        elif ind == 23:
            self.layer2[3].conv3 = layer

        # layer3
        elif ind == 24:
            self.layer3[0].conv1 = layer
        elif ind == 25:
            self.layer3[0].conv2 = layer
        elif ind == 26:
            self.layer3[0].conv3 = layer
        elif ind == 27:
            self.layer3[0].downsample[0] = layer

        elif ind == 28:
            self.layer3[1].conv1 = layer
        elif ind == 29:
            self.layer3[1].conv2 = layer
        elif ind == 30:
            self.layer3[1].conv3 = layer

        elif ind == 31:
            self.layer3[2].conv1 = layer
        elif ind == 32:
            self.layer3[2].conv2 = layer
        elif ind == 33:
            self.layer3[2].conv3 = layer

        elif ind == 34:
            self.layer3[3].conv1 = layer
        elif ind == 35:
            self.layer3[3].conv2 = layer
        elif ind == 36:
            self.layer3[3].conv3 = layer

        elif ind == 37:
            self.layer3[4].conv1 = layer
        elif ind == 38:
            self.layer3[4].conv2 = layer
        elif ind == 39:
            self.layer3[4].conv3 = layer

        elif ind == 40:
            self.layer3[5].conv1 = layer
        elif ind == 41:
            self.layer3[5].conv2 = layer
        elif ind == 42:
            self.layer3[5].conv3 = layer

        #layer4
        elif ind == 43:
            self.layer4[0].conv1 = layer
        elif ind == 44:
            self.layer4[0].conv2 = layer
        elif ind == 45:
            self.layer4[0].conv3 = layer
        elif ind == 46:
            self.layer4[0].downsample[0] = layer

        elif ind == 47:
            self.layer4[1].conv1 = layer
        elif ind == 48:
            self.layer4[1].conv2 = layer
        elif ind == 49:
            self.layer4[1].conv3 = layer

        elif ind == 50:
            self.layer4[2].conv1 = layer
        elif ind == 51:
            self.layer4[2].conv2 = layer
        elif ind == 52:
            self.layer4[2].conv3 = layer

        else:
            raise Exception('BadIndex')

    def get_layer_by_ind(self, ind):

        if ind == 0:
            return self.conv1

        #layer1
        elif ind == 1:
            return self.layer1[0].conv1
        elif ind == 2:
            return self.layer1[0].conv2
        elif ind == 3:
            return self.layer1[0].conv3
        elif ind == 4:
            return self.layer1[0].downsample[0]

        elif ind == 5:
            return self.layer1[1].conv1
        elif ind == 6:
            return self.layer1[1].conv2
        elif ind == 7:
            return self.layer1[1].conv3

        elif ind == 8:
            return self.layer1[2].conv1
        elif ind == 9:
            return self.layer1[2].conv2
        elif ind == 10:
            return self.layer1[2].conv3

        # layer2
        elif ind == 11:
            return self.layer2[0].conv1
        elif ind == 12:
            return self.layer2[0].conv2
        elif ind == 13:
            return self.layer2[0].conv3
        elif ind == 14:
            return self.layer2[0].downsample[0]

        elif ind == 15:
            return self.layer2[1].conv1
        elif ind == 16:
            return self.layer2[1].conv2
        elif ind == 17:
            return self.layer2[1].conv3

        elif ind == 18:
            return self.layer2[2].conv1
        elif ind == 19:
            return self.layer2[2].conv2
        elif ind == 20:
            return self.layer2[2].conv3

        elif ind == 21:
            return self.layer2[3].conv1
        elif ind == 22:
            return self.layer2[3].conv2
        elif ind == 23:
            return self.layer2[3].conv3

        # layer3
        elif ind == 24:
            return self.layer3[0].conv1
        elif ind == 25:
            return self.layer3[0].conv2
        elif ind == 26:
            return self.layer3[0].conv3
        elif ind == 27:
            return self.layer3[0].downsample[0]

        elif ind == 28:
            return self.layer3[1].conv1
        elif ind == 29:
            return self.layer3[1].conv2
        elif ind == 30:
            return self.layer3[1].conv3

        elif ind == 31:
            return self.layer3[2].conv1
        elif ind == 32:
            return self.layer3[2].conv2
        elif ind == 33:
            return self.layer3[2].conv3

        elif ind == 34:
            return self.layer3[3].conv1
        elif ind == 35:
            return self.layer3[3].conv2
        elif ind == 36:
            return self.layer3[3].conv3

        elif ind == 37:
            return self.layer3[4].conv1
        elif ind == 38:
            return self.layer3[4].conv2
        elif ind == 39:
            return self.layer3[4].conv3

        elif ind == 40:
            return self.layer3[5].conv1
        elif ind == 41:
            return self.layer3[5].conv2
        elif ind == 42:
            return self.layer3[5].conv3

        #layer4
        elif ind == 43:
            return self.layer4[0].conv1
        elif ind == 44:
            return self.layer4[0].conv2
        elif ind == 45:
            return self.layer4[0].conv3
        elif ind == 46:
            return self.layer4[0].downsample[0]

        elif ind == 47:
            return self.layer4[1].conv1
        elif ind == 48:
            return self.layer4[1].conv2
        elif ind == 49:
            return self.layer4[1].conv3

        elif ind == 50:
            return self.layer4[2].conv1
        elif ind == 51:
            return self.layer4[2].conv2
        elif ind == 52:
            return self.layer4[2].conv3

        else:
            raise Exception('BadIndex')


def identity_resnet50(**kwargs):
    """Constructs a ResNet-50 model.

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(Bottleneck, [3, 4, 6, 3], **kwargs)
    return model
