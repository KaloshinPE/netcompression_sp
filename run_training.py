import argparse
import os
import random
import shutil
import time
import warnings
import pandas as pd

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

import model_utils as mu
import layer_decomposition as ld
from utils import bcolors

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')

parser.add_argument('data', metavar='DIR',
                    help='path to dataset')
parser.add_argument('--results', metavar='DIR', required=True,
                    help='where to store results')
parser.add_argument('--arch', metavar='PATH', required=True,
                    help='file where pathes to decomposed layers are set')
parser.add_argument('--arch_folder', metavar='PATH', required=True,
                    help='folder with decomposed layers')
parser.add_argument('--epochs', default=2, type=int, metavar='N',
                    help='number of fine-tuning epochs to run on each layer')
parser.add_argument('--prior_validation', action='store_true',
                    help='validate model before any training')
parser.add_argument('--debug', action='store_true',
                    help='train only on pring-freq batches each epoch')

parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('-b', '--batch-size', default=64, type=int,
                    metavar='N',
                    help='mini-batch size (default: 256), this is the total '
                         'batch size of all GPUs on the current node when '
                         'using Data Parallel or Distributed Data Parallel')
parser.add_argument('--lr', '--learning-rate', default=0.0001, type=float,
                    metavar='LR', help='initial learning rate', dest='lr')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--wd', '--weight-decay', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)',
                    dest='weight_decay')
parser.add_argument('-p', '--print-freq', default=10, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--world-size', default=-1, type=int,
                    help='number of nodes for distributed training')
parser.add_argument('--rank', default=-1, type=int,
                    help='node rank for distributed training')
parser.add_argument('--dist-url', default='tcp://224.66.41.62:23456', type=str,
                    help='url used to set up distributed training')
parser.add_argument('--dist-backend', default='nccl', type=str,
                    help='distributed backend')
parser.add_argument('--seed', default=None, type=int,
                    help='seed for initializing training. ')
parser.add_argument('--gpu', default=None, type=int,
                    help='GPU id to use.')
parser.add_argument('--multiprocessing-distributed', action='store_true',
                    help='Use multi-processing distributed training to launch '
                         'N processes per node, which has N GPUs. This is the '
                         'fastest way to use PyTorch for either single node or '
                         'multi node data parallel training')
args = parser.parse_args()
time_start = time.time()


ngpus_per_node = torch.cuda.device_count()
# we want to print things or save checkpoints only from main thread
main_thread = False
if not args.multiprocessing_distributed or (args.multiprocessing_distributed
        and args.rank % ngpus_per_node == 0):
    main_thread = True

def printf(*args, **kwargs):
    if main_thread:
        print(*args, **kwargs)


def time_to_str(time):
    time = int(time)
    timestr = f'{time // 3600:3d}h'.strip() + f':{time % 3600 // 60:2d}m:{int(time % 60):2d}s'
    return timestr


def main():

    if not os.path.exists(args.results):
        os.makedirs(args.results)
        with open(os.path.join(args.results, 'log.tsv'), 'w') as f:
            f.write('layer\tepoch\tacc1\tacc5\n')

    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. '
                      'This will turn on the CUDNN deterministic setting, '
                      'which can slow down your training considerably! '
                      'You may see unexpected behavior when restarting '
                      'from checkpoints.')

    if args.gpu is not None:
        warnings.warn('You have chosen a specific GPU. This will completely '
                      'disable data parallelism.')

    if args.dist_url == "env://" and args.world_size == -1:
        args.world_size = int(os.environ["WORLD_SIZE"])

    args.distributed = args.world_size > 1 or args.multiprocessing_distributed

    while True:
        log = pd.read_csv(os.path.join(args.results, 'log.tsv'), sep='\t')
        if len(log) == 0:
            checkpoint_file, checkpoint_ind, decomposing_ind = None, -1, 0
        else:
            last_processed_ind = max(log['layer'].values)
            log = log.dropna() # removed all non-decomposed layers
            if len(log) > 0:
                last_decomposed_ind = max(log['layer'].values)
                log = log[log['layer'] == last_decomposed_ind] # look only at last decomposition
                last_decomposed_epoch = int(max(log['epoch'].values))

                checkpoint_file = os.path.join(args.results, str(last_decomposed_ind),
                                               f'checkpoint_{last_decomposed_epoch}.pth.tar')
                if last_decomposed_epoch >= args.epochs - 1:
                    checkpoint_ind, decomposing_ind = last_decomposed_ind, last_processed_ind+1
                else:
                    checkpoint_ind, decomposing_ind = last_processed_ind, last_processed_ind
            else:
                checkpoint_file, checkpoint_ind, decomposing_ind = None, -1, last_processed_ind + 1


        if decomposing_ind > 52:
            printf('resnet50 is fully decomposed!')
            return 

        if args.multiprocessing_distributed:
            # Since we have ngpus_per_node processes per node, the total world_size
            # needs to be adjusted accordingly
            args.world_size = ngpus_per_node * args.world_size
            # Use torch.multiprocessing.spawn to launch distributed processes: the
            # main_worker process function
            mp.spawn(main_worker, nprocs=ngpus_per_node, args=(ngpus_per_node, args, checkpoint_file, checkpoint_ind, decomposing_ind))
        else:
            # Simply call main_worker function
            main_worker(args.gpu, ngpus_per_node, args, checkpoint_file, checkpoint_ind, decomposing_ind)


def main_worker(gpu, ngpus_per_node, args, checkpoint_file, checkpoint_ind, decomposing_ind):
    args.gpu = gpu

    if args.gpu is not None:
        printf("Use GPU: {} for training".format(args.gpu))

    if args.distributed:
        if args.dist_url == "env://" and args.rank == -1:
            args.rank = int(os.environ["RANK"])
        if args.multiprocessing_distributed:
            # For multiprocessing distributed training, rank needs to be the
            # global rank among all the processes
            args.rank = args.rank * ngpus_per_node + gpu
        dist.init_process_group(backend=args.dist_backend, init_method=args.dist_url,
                                world_size=args.world_size, rank=args.rank)
    # create model
    
    with open(args.arch, 'r') as f:
        decomposition_pathes = []
        for line in f:
            if line.startswith('#'):
                break
            if len(line) == 1: # if it's empty
                decomposition_pathes.append(None)
            else:
                decomposition_pathes.append(os.path.join(args.arch_folder, line[:-1]))

    if decomposition_pathes[decomposing_ind] is None:
        with open(os.path.join(args.results, 'log.tsv'), 'a') as f:
            f.write(f"{decomposing_ind}\t\t\t\n")
            return

            
    model = mu.make_original_model()
    for i in range(checkpoint_ind+1):
        if decomposition_pathes[i] is not None:
            layer = ld.layer_from_pickle(decomposition_pathes[i])
            model.set_layer_by_ind(layer, i)

    optimizer_state = None
    if checkpoint_file is not None:
        printf(f"=> loading checkpoint '{checkpoint_file}'")
        checkpoint = torch.load(checkpoint_file)
        start_epoch = checkpoint['epoch']
        if args.gpu is not None:
            # best_acc1 may be from a checkpoint from a different GPU
            best_acc1 = best_acc1.to(args.gpu)
        model.load_state_dict(checkpoint['state_dict'])
        if checkpoint_ind == decomposing_ind:
            optimizer_state = checkpoint['optimizer']
        printf("=> loaded checkpoint '{}' (epoch {})"
              .format(checkpoint_file, checkpoint['epoch']))

    if checkpoint_ind == -1 or decomposing_ind > checkpoint_ind:
        start_epoch = 0

    if decomposing_ind > checkpoint_ind:
        new_layer = ld.layer_from_pickle(decomposition_pathes[decomposing_ind])
        model.set_layer_by_ind(new_layer, decomposing_ind)

    for i in range(decomposing_ind + 1, 53):
        for param in model.get_layer_by_ind(i).parameters():
            param.requires_grad = False

    for i in range(decomposing_ind+1):
        layer = model.get_layer_by_ind(i)
        if hasattr(layer, 'mask'):
            layer.mask.requires_grad = False


    if args.debug:
        for i in range(0, 53):
            layer = model.get_layer_by_ind(i)
            printf(f'{i} -- {layer}: ')
            for param in layer.parameters():
                printf(f'{param.shape}: trainable {param.requires_grad}') 
            printf('='*40)

        printf(model)



    if args.distributed:
        # For multiprocessing distributed, DistributedDataParallel constructor
        # should always set the single device scope, otherwise,
        # DistributedDataParallel will use all available devices.
        if args.gpu is not None:
            torch.cuda.set_device(args.gpu)
            model.cuda(args.gpu)
            # When using a single GPU per process and per
            # DistributedDataParallel, we need to divide the batch size
            # ourselves based on the total number of GPUs we have
            args.batch_size = int(args.batch_size / ngpus_per_node)
            args.workers = int((args.workers + ngpus_per_node - 1) / ngpus_per_node)
            model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[args.gpu])
        else:
            model.cuda()
            # DistributedDataParallel will divide and allocate batch_size to all
            # available GPUs if device_ids are not set
            model = torch.nn.parallel.DistributedDataParallel(model)
    elif args.gpu is not None:
        torch.cuda.set_device(args.gpu)
        model = model.cuda(args.gpu)
    else:
        # DataParallel will divide and allocate batch_size to all available GPUs
        model = torch.nn.DataParallel(model).cuda()

    # define loss function (criterion) and optimizer
    criterion = nn.CrossEntropyLoss().cuda(args.gpu)

    optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                momentum=args.momentum,
                                weight_decay=args.weight_decay)
    if optimizer_state is not None:
        optimizer.load_state_dict(optimizer_state)

    # optionally resume from a checkpoint
    # load model here, may be needed to use args.gpu TODO

    cudnn.benchmark = True

    # Data loading code
    traindir = os.path.join(args.data, 'train')
    valdir = os.path.join(args.data, 'valid')
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    train_dataset = datasets.ImageFolder(
        traindir,
        transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ]))

    if args.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    else:
        train_sampler = None

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=(train_sampler is None),
        num_workers=args.workers, pin_memory=True, sampler=train_sampler)

    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(valdir, transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    if args.prior_validation:
        printf('prior evaluation')
        validate(val_loader, model, criterion, args)
        printf(f'running fine tuning of {decomposing_ind} layer from {start_epoch} epoch')

 
    for epoch in range(start_epoch, args.epochs):
        if args.distributed:
            train_sampler.set_epoch(epoch)

        adjust_learning_rate(optimizer, epoch, args)

        # train for one epoch
        train(train_loader, model, criterion, optimizer, epoch, args, decomposing_ind, None)

        # evaluate on validation set
        acc1, acc5 = validate(val_loader, model, criterion, args)

        if not args.multiprocessing_distributed or (args.multiprocessing_distributed
                and main_thread):
            save_checkpoint({
                'epoch': epoch,
                'arch': args.arch,
                'state_dict': model.module.state_dict(),
                'acc1': acc1,
                'optimizer' : optimizer.state_dict(),
            }, os.path.join(args.results, f'{decomposing_ind}/checkpoint_{epoch}.pth.tar'))
            with open(os.path.join(args.results, 'log.tsv'), 'a') as f:
                f.write(f"{decomposing_ind}\t{epoch}\t{acc1}\t{acc5}\n")


        if args.debug:
            printf('visually controll sparsity perservance')
            for i in range(decomposing_ind+1):
                layer = model.module.get_layer_by_ind(i)
                if hasattr(layer, 'sparse'):
                    printf(layer.sparse.weight.detach().cpu().numpy().reshape(-1)[:10])


def train(train_loader, model, criterion, optimizer, epoch, args, decomposing_ind, lr_scheduler=None):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix=f"[Layer: {decomposing_ind} -- Epoch: {epoch}]")

    # switch to train mode
    model.train()

    end = time.time()
    for i, (images, target) in enumerate(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        if args.gpu is not None:
            images = images.cuda(args.gpu, non_blocking=True)
        target = target.cuda(args.gpu, non_blocking=True)

        # compute output
        output = model(images)
        loss = criterion(output, target)

        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        losses.update(loss.item(), images.size(0))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        if lr_scheduler is not None:
            lr_scheduler.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            progress.display(i)
            if args.debug and i > 0:
                break


def validate(val_loader, model, criterion, args):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time, losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            output = model(images)
            loss = criterion(output, target)

            # measure accuracy and record loss
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                progress.display(i)
                if args.debug and i > 0:
                    break

        # TODO: this should also be done with the ProgressMeter
        printf(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))

    return top1.avg, top5.avg


def save_checkpoint(state, filename='checkpoint.pth.tar'):
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    torch.save(state, filename)


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix
        self.num_batches = num_batches

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        for meter in self.meters:
            if meter.name == 'Time':
                cur_time = meter.sum
                rest_nbatches = self.num_batches - batch - 1
                rest_time = rest_nbatches*meter.avg
                printf(f'{bcolors.OKGREEN}{time_to_str(time.time() - time_start)}{bcolors.ENDC} - ', end='')
                printf(f'[ {time_to_str(cur_time)}/{time_to_str(rest_time + cur_time)} ] ', end='')
                break
             
        if args.debug:
            printf('\t'.join(entries))
        else:
            printf('\t'.join(entries), end='\r')

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def adjust_learning_rate(optimizer, epoch, args):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (0.5 ** epoch)
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


if __name__ == '__main__':
    main()
