#!/bin/bash
decompositions_dir=${1%*/} 
epochs_number=$2
for arch in $(ls $decompositions_dir|grep arch)
do
	#echo ${arch%.*}
	python run_training.py /home/pavel/data/ILSVRC/Data/CLS-LOC/ --results ${decompositions_dir}_results/${arch%.*} --arch $decompositions_dir/$arch --arch_folder /media/pavel/storage/decomposed_layers --epochs $epochs_number
done;
