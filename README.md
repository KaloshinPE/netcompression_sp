# Layer decomposition
First of all, `python run_layer_decomposition.py /some/path/decomposed_layers` should be run. This command will decompose all the layers of resnet50 in parallel with ranks increasing till pre-defined error bound is reached.
All the intermediate decompositions will be stored. Note that full decomposition for 0.2 error bound takes little bit longer that a week on core i9 and requires 7.1Gb storage space.

# Decomposition choice 
`python choose_decomposed_layers.py /media/pavel/storage/decomposed_layers --error .35 --sparsity 0.01` this command will give you a txt file with relative pathes to required decompositions.

Optionally run:
`python pack_decompositions.py /media/pavel/storage/decomposed_layers --to-store /media/pavel/storage/dump_layers --chosen layers__sparsity_0.01__error_0.35.txt`
This will copy all the selected decompositions to the separate folder (<150Mb, much easier to send to your GPU server)

# Training
`python run_training.py /home/pavel/data/ILSVRC/Data/CLS-LOC/ --results /media/pavel/storage/decomposition_results --arch layers__sparsity_0.01__error_0.35.txt --arch_folder /media/pavel/storage/dump_layers/ --prior_validation` for training on one GPU

# Data format
Pytorch resnet50 pretrained on imagenet is being decomposed. \
Path to imagenet dataset contains `train` and `valid` folders, each of them has the format of torchvision [image folder](https://pytorch.org/docs/stable/torchvision/datasets.html#imagefolder).

## Original parameters
Acc@1 76.130 Acc@5 92.862

# One layer tests
It is interesting to test how one single layer can withstand the decomposition error. For this purpose, one needs to form arch files for different decomposition errors as follows:\
`python choose_decompositions_for_one_layer.py 51 --results /media/pavel/storage/decomposed_layers/ --sparsity .01 --out /media/pavel/storage/decomposition_results/51_decompositions --error_low 0.35 error_high 0.7--error_step 0.05
`\
This will take the decompositions from error_low to error_high from decomposition folder with pairwise difference more than error_step. The first argument corresponds to layer number.

Then just run fine tuning:\
`./run_one_layer_test.sh /media/pavel/storage/decomposition_results/51_decompositions/ 5`\
Last argument is for epoch number.

Results can be obtained from logs.

