from fastai.vision import *
from fastai.basics import *


def get_databunch(path, labels_map, image_size, train_batch_size, val_batch_size):
    """
    returns fastai databunch for imagenet
    :param path: path do imagenet dataset root, where train, val and test folders are situated. Each such folder
    (except maybe test, which may contain just images) contains one folder for each class containing images of the
    corresponding class.
    :param labels_map: relative path from dataset root to text file where mapping from folder names to real classes
    names is presented.
    :param image_size: size of images for network input
    :param train_batch_size: train batch size
    :param val_batch_size: validation batch size
    :return: fastai dataset
    """
    labels_map_path = os.path.join(path, labels_map)
    s = open(labels_map_path, 'r').read()
    labels_map = eval(s)

    tfms = get_transforms()
    dataset = (ImageList.from_folder(path)
               .split_by_folder()
               .label_from_func(
        func=lambda o: labels_map[(o.parts if isinstance(o, Path) else o.split(os.path.sep))[-2]])
               .add_test_folder()
               .transform(tfms, size=image_size))

    return dataset

