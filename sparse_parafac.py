import numpy as np
import warnings

import tensorly as tl
from tensorly.random import check_random_state
from tensorly.base import unfold
from tensorly.kruskal_tensor import kruskal_to_tensor
from tensorly.tenalg import khatri_rao

from tensorly.decomposition.candecomp_parafac import normalize_factors, initialize_factors


def limit_cardinality(tensor, card, block_shape=None):
    if card >= np.prod(tensor.shape):
        return tensor
    bound = np.sort(np.abs(tensor.flatten()))[-card]
    tensor[np.abs(tensor) < bound] = 0
    return tensor


def sparse_parafac(tensor, rank, n_iter_max=100, init='svd', svd='numpy_svd', tol=1e-8, card=0.1,
            orthogonalise=False, random_state=None, verbose=False, return_errors=False, block_shape=None):
    """CANDECOMP/PARAFAC decomposition via alternating least squares (ALS)

    Computes a rank-`rank` decomposition of `tensor` [1]_ such that,

        ``tensor = [| factors[0], ..., factors[-1] |]``.

    Parameters
    ----------
    tensor : ndarray
    rank  : int
        Number of components.
    n_iter_max : int
        Maximum number of iteration
    init : {'svd', 'random'}, optional
        Type of factor matrix initialization. See `initialize_factors`.
    svd : str, default is 'numpy_svd'
        function to use to compute the SVD, acceptable values in tensorly.SVD_FUNS
    tol : float, optional
        (Default: 1e-6) Relative reconstruction error tolerance. The
        algorithm is considered to have found the global minimum when the
        reconstruction error is less than `tol`.
    card: desired cardinality for sparse component. can be float (fraction) or int number
    random_state : {None, int, np.random.RandomState}
    verbose : int, optional
        Level of verbosity
    block_shape: shape of block for block sparsity
    return_errors : bool, optional
        Activate return of iteration errors


    Returns
    -------
    factors : ndarray list
        List of factors of the CP decomposition element `i` is of shape
        (tensor.shape[i], rank)
    errors : list
        A list of reconstruction errors at each iteration of the algorithms.

    References
    ----------
    .. [1] tl.G.Kolda and B.W.Bader, "Tensor Decompositions and Applications",
       SIAM REVIEW, vol. 51, n. 3, pp. 455-500, 2009.
    """
    if orthogonalise and not isinstance(orthogonalise, int):
        orthogonalise = n_iter_max

    factors = initialize_factors(tensor, rank, init=init, svd=svd, random_state=random_state)
    rec_errors = []
    norm_tensor = tl.norm(tensor, 2)

    if type(card) in [float]:
        card = int(card*np.prod(tensor.shape))
    else:
        card = int(card)

    S = tl.zeros_like(tensor)

    for iteration in range(n_iter_max):
        cur_tensor = tensor - S

        if orthogonalise and iteration <= orthogonalise:
            factor = [tl.qr(factor)[0] for factor in factors]

        for mode in range(tl.ndim(cur_tensor)):
            pseudo_inverse = tl.tensor(np.ones((rank, rank)), **tl.context(cur_tensor))
            for i, factor in enumerate(factors):
                if i != mode:
                    pseudo_inverse = pseudo_inverse*tl.dot(tl.transpose(factor), factor)
            factor = tl.dot(unfold(cur_tensor, mode), khatri_rao(factors, skip_matrix=mode))
            factor = tl.transpose(tl.solve(tl.transpose(pseudo_inverse), tl.transpose(factor)))
            factors[mode] = factor
            reconstructed = kruskal_to_tensor(factors)
            S = limit_cardinality(tensor - reconstructed, card, block_shape)

        if tol:
            rec_error = tl.norm(tensor - reconstructed - S, 2) / norm_tensor
            rec_errors.append(rec_error)

            if iteration > 1:
                if verbose:
                    print('reconstruction error={}, variation={}.'.format(
                        rec_errors[-1], rec_errors[-2] - rec_errors[-1]))

                if tol and abs(rec_errors[-2] - rec_errors[-1]) < tol:
                    if verbose:
                        print('converged in {} iterations.'.format(iteration))
                    break

    if return_errors:
        return factors, S, rec_errors
    else:
        return factors, S
