import pickle
import pandas as pd
import os
import argparse
from multiprocessing import Pool, cpu_count
import numpy as np
import time
import yaml

from layer_decomposition import cp_decomposition_conv_layer
from tensorly.decomposition import parafac
from sparse_parafac import sparse_parafac
import model_utils as mu
from utils import bcolors

parser = argparse.ArgumentParser(description='Layer decomposition')
parser.add_argument('results', metavar='DIR',
                    help='path to store decomposition results')
parser.add_argument('--nprocs', type=int, default=-1,
                    help='number of processes to use, -1 for all available')
parser.add_argument('--err', type=float, default=.2,
                    help='error to stop decomposition process')
parser.add_argument('--sparsity_coeff', type=float, default=.01,
                    help='error to stop decomposition process')
parser.add_argument('--indstart', type=int, default=0,
                    help='layer from witch to start decomposition')
args = parser.parse_args()

if args.nprocs == -1:
    args.nprocs = cpu_count()

print(f'{args.nprocs} processes will run')

def decompose_layer(layer, ind, error_bound, sparsity_coeff, store_step=.01, verbose_n=15):
# def decompose_layer(params, store_step=.01):
    """
    ind: number of layer in model
    error_bound: error when stop rank increase
    sparsity_coeff: percentage of nonzero elements. 0 for non sparse decomposition
    store_step: how far from each other should be the stored points in terms of error
    verbose_n: when to print the status
    """
#     layer, ind, error, sparsity_coeff = params

    print(f'{bcolors.OKBLUE} {ind} layer decomposition started {bcolors.ENDC}')
    layer_dir = os.path.join(args.results, str(ind))
    os.makedirs(layer_dir, exist_ok=True)
    metadata = os.path.join(layer_dir, 'meta.tsv') 
    layer_metadata = os.path.join(layer_dir, 'layer_meta.yaml') 
    original_layer = os.path.join(layer_dir, 'original_layer.pkl') 
    if not os.path.isfile(metadata):
        with open(metadata, 'w') as f:
            # creating tsv with nice column names
            f.write('\t'.join(['id', 'rank', 'sparsity', 'compression', 'n_sparse_params', 'error\n']))
        with open(layer_metadata, 'w') as f:
            yaml.dump({'shape': layer_weight.shape, 'type': type(layer)}, f)
        with open(original_layer, 'wb') as f:
            pickle.dump(layer, f)

    old_meta = pd.read_csv(metadata, sep='\t')
    old_meta = old_meta[old_meta['sparsity'] == sparsity_coeff] # chose only samples related to this decomposition
    if len(old_meta[old_meta['error'] < error_bound]) > 0: # if we already finished decomposing with given error bound 
        print(f'{bcolors.OKGREEN} {ind} layer is already decomposed {bcolors.ENDC}')
        return

    begin_time = time.perf_counter()

    sparse = True if sparsity_coeff > 0 else False

    # if the error is changing too slow, lets increment the rank step to do everything faster
    rank_increase_diff_thresh = 0.001

    last_stored_error = None
    layer_weight = layer.weight.data.cpu().numpy()
    last_rank = None
    rank_step = 1
    last_err = None
    for i, rank in enumerate(range(2, int(3*np.prod(layer.weight.shape)/sum(layer.weight.shape)))):
        
        if len(old_meta[old_meta['rank'] >= rank]) > 0: # if we already did this decomposition
            continue

        if last_rank is None:
            last_rank = rank
        elif rank - last_rank < rank_step:
                continue 

        if sparse:
            (last, first, vertical, horizontal), S, errs = sparse_parafac(layer_weight, rank=rank,
                                                                          init='random',
                                                                 return_errors=True, card=sparsity_coeff)
            n_params = (sum(layer_weight.shape)*rank + (len(layer_weight.shape)+1)*sparsity_coeff*np.prod(layer_weight.shape))
            compression = np.prod(layer_weight.shape)/n_params
        else:
            (last, first, vertical, horizontal), errs = parafac(layer_weight, rank=rank, init='random',
                                                                 return_errors=True)
            S = None
            n_params = sum(layer_weight.shape)*rank
            compression = np.prod(layer_weight.shape)/n_params
        err = errs[-1]
        

        if last_err is not None and abs(last_err - err) < rank_increase_diff_thresh:
            rank_step += 1


        if last_stored_error is None or last_stored_error - err > store_step:
            new_id = len(os.listdir(layer_dir))
            with open(os.path.join(layer_dir, f'{new_id}.pkl'), 'wb') as f:
                pickle.dump((last, first, vertical, horizontal, S), f)

            with open(metadata, 'a') as f:
                n_sparse = 0
                if sparse:
                    n_sparse = len(S.nonzero()[0])
                f.write('\t'.join([f'{new_id}', f'{rank}', f'{sparsity_coeff}', f'{compression}',
                                   f'{n_sparse}',
                                   f'{err}\n']))
            last_stored_error = err

        if i % verbose_n == 0:
            time_delta = time.perf_counter() - begin_time
            print('\t'.join([f'+++{ind}: ', f'\trank={rank}', f'step={rank_step}', f'compression={compression:.3f}',
                     f'err={err:.3f}', f'time={time_delta // 3600}h:{time_delta % 3600 // 60}m:{int(time_delta % 60)}s', 
                             f'last stored error = {last_stored_error:.3f}']))
        
        last_rank = rank
        last_err = err

        if err < error_bound:
            break

    print(f'{bcolors.OKGREEN} {ind} layer decomposition finished {bcolors.ENDC}')


def main():
    model = mu.make_original_model()
    # 53 is number of decomposable layers in resnet50
    layers_to_decompose = [(model.get_layer_by_ind(ind), ind, args.err, args.sparsity_coeff) for ind in
                           range(args.indstart, 53)] 

    with Pool(args.nprocs) as pool:
        pool.starmap(decompose_layer, layers_to_decompose)
#     pool =  Pool(args.nprocs)
#     pool.map(decompose_layer, layers_to_decompose)

if __name__ == '__main__':
    main()
