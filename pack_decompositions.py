import argparse
import os
import shutil

from utils import bcolors

parser = argparse.ArgumentParser(description='Choose decomposed layers verisons')
parser.add_argument('results', metavar='DIR',
                    help='path where decomposition results')
parser.add_argument('--to-store', metavar='DIR', required=True,
                    help='path where chosen decomposition should be stored')
parser.add_argument('--chosen', metavar='PATH', required=True, 
                    help='file with chosen weights relative pathes')
args = parser.parse_args()


if __name__ == '__main__':
    with open(args.chosen) as f:
        chosen = [x[:-1] for x in f.readlines()]

    os.makedirs(args.to_store)


    for decomposition in chosen:
        layer_no = decomposition.split('/')[0]
        old_folder = os.path.join(args.results, layer_no) 
        new_folder = os.path.join(args.to_store, layer_no) 
        os.makedirs(new_folder)
        for elem in ['layer_meta.yaml', 'original_layer.pkl']:
            shutil.copyfile(os.path.join(old_folder, elem), os.path.join(new_folder, elem)) 
        shutil.copyfile(os.path.join(args.results, decomposition), os.path.join(args.to_store, decomposition)) 
