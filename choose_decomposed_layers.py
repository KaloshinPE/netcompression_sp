import argparse
import os
import pandas as pd

from utils import bcolors

parser = argparse.ArgumentParser(description='Choose decomposed layers verisons')
parser.add_argument('results', metavar='DIR',
                    help='path where decomposition results')
parser.add_argument('--out', type=str, help='where to store results')
parser.add_argument('--sparsity', type=float, default=0.01, help='required sparsity level')
parser.add_argument('--error', type=float, default=0.35, help='decomposition error bound')
args = parser.parse_args()

def main():

    if args.out is None:
        args.out = f'layers__sparsity_{args.sparsity}__error_{args.error}.txt' 

    if os.path.isfile(args.out):
        while True:
            print (f'{bcolors.WARNING}Output file exists. are you shure you want to overwrite it? (Y/n) {bcolors.ENDC}')
            ret = input()
            if ret in ['Y', 'y']:
                os.remove(args.out)
                break
            if ret in ['N', 'n']:
                return

    for i in range(53):
        layer_path = os.path.join(args.results, str(i))
        weight_path = None
        if os.path.exists(layer_path):
            metapath = os.path.join(layer_path, 'meta.tsv')
            meta = pd.read_csv(metapath, sep='\t')
            meta = meta[(meta['sparsity'] == args.sparsity) & (meta['error'] < args.error)]
            if len(meta) > 0:
                chosen_meta = meta.iloc[meta['rank'].values.argmin()]
                ind = int(chosen_meta['id'])
                weight_path = f'{i}/{ind}.pkl'

        if weight_path is None:
            print(f'required decompositions for layer {i} do not exist!')
            weight_path = ''

        with open(args.out, 'a') as f:
            f.write(weight_path + '\n')
#     with open(args.out, 'a') as f:
#         f.write(f'# sparsity={args.sparsity}\n')
#         f.write(f'# error={args.error}')

if __name__ == '__main__':
    main()

        

     
